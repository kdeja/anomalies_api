import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")
import uproot
import numpy as np
import pandas as pd
import pickle
import requests
import configparser
import sys


import tensorflow.keras
from tensorflow.keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Lambda
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Dropout, Activation, LeakyReLU, SimpleRNN,BatchNormalization
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K
from tensorflow.keras.models import load_model

from tensorflow.python.keras.backend import set_session
from flask import request
import flask
import io

sess = tensorflow.Session()
graph = tensorflow.get_default_graph()

app = flask.Flask(__name__)
graph = tensorflow.get_default_graph()

model = None
sc = None
selected_colums = None
model_dir = None
save_directory = None
datalake_address = None
datalake_directory = None
tmp_trending_list = []
tmp_trending_evs_list = []

def load_config(config_file = "config_files/config.ini"):
    global datalake_address 
    global model_dir
    global save_directory
    global datalake_directory
    config = configparser.ConfigParser()
    config.read(config_file)
    model_dir = config['DEFAULT']['model_dir']
    save_directory = config['DEFAULT']['tmp_save_directory']
    datalake_address = config['DEFAULT']['datalake_address']
    datalake_directory = config['DEFAULT']['datalake_directory']
    
def load_local_model():
    global model
    global sc
    global selected_colums
    print("loading model and scalers from:"+model_dir)
    set_session(sess)
    with open(model_dir+"columns_names.pkl","rb") as f:
        selected_colums = pickle.load(f)
    sc = pickle.load(open(model_dir+"scaler.sav", 'rb'))
    model = load_model(model_dir+"model.h5")
    
def download_data(start_interval, end_interval):
    global tmp_trending_list
    global tmp_trending_evs_list
    print("downloading data")
    next_interval = start_interval
    try:
        while(next_interval<end_interval):
            trending = requests.get(datalake_address+datalake_directory+'trendings/'+str(next_interval))
            print(datalake_address+datalake_directory+'trendings/'+str(next_interval))
            tmp_trending_list.append(next_interval)
            with open(save_directory+"trending_"+str(next_interval)+".root",'wb') as tmpfile:
                tmpfile.write(trending.content)
            next_interval = int(trending.headers['Valid-Until'])+1

        next_interval = start_interval
        while(next_interval<end_interval):
            trending_evs = requests.get(datalake_address+datalake_directory+'trendings_EVS/'+str(next_interval))
            tmp_trending_evs_list.append(next_interval)
            with open(save_directory+"trending_evs_"+str(next_interval)+".root",'wb') as tmpfile:
                tmpfile.write(trending_evs.content)
            next_interval = int(trending_evs.headers['Valid-Until'])+1
    except:
        return(-1)
    return(0)

    
def data_preprocessing():
    data_total = []
    for index in tmp_trending_list:       
    ###Maybe include possibility for different lengths of intervals for trendings and trendigns_evs?
        trending_root = uproot.open(save_directory+"trending_"+str(index)+".root")
        trending_evc_root = uproot.open(save_directory+"trending_evs_"+str(index)+".root")
        data = trending_root['tpcQA'].pandas.df()
        data_concat = pd.concat([data,trending_evc_root['trending;1'].pandas.df()["interactionRate"]],axis=1)
        data_selected = data_concat[selected_colums]
        data_scaled = sc.transform(data_selected)
        data_total.append(data_scaled)
    data_total = np.concatenate(data_total)
    return(data_total)

def get_prediction():
    data = data_preprocessing()
    print("predicting quality")
    return(np.mean(np.power(data - model.predict(data), 2), axis=1))

def clean_directory():
    global tmp_trending_list
    global tmp_trending_evs_list
    tmp_trending_list = []
    tmp_trending_evs_list = []
    filenames = os.listdir(save_directory)
    for file in filenames:
        os.remove(save_directory+file)   

@app.route('/predict', methods=['GET', 'POST'])
def predict():
    data = {"success": False}
    if request.method == 'GET':
        x = request.args['data']
#         trending_id = request.args['trending_id']
#         trending_evc_id = request.args['trending_evc_id']

    elif request.method == 'POST':
        x = request.args['data']
#         trending_id = request.args['trending_id']
#         trending_evc_id = request.args['trending_evc_id']
        
    data = x.split(',')
    start_interval = int(data[0])
    end_interval = int(data[1])
    
    clean_directory()
    download_result = download_data(start_interval,end_interval)
    if(download_result !=0):
        return("No data for a given time period")
    
    with graph.as_default():
        set_session(sess)
        pred = get_prediction()
        return(str(pred))


if __name__ == '__main__':
    load_config()
    load_local_model()
    app.run(port=sys.argv[1])
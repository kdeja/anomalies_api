#include <iostream>
#include <stdio.h>
#include <string>
#include <curl/curl.h>

using namespace std;

size_t CurlWrite_CallbackFunc_StdString(void *contents, size_t size, size_t nmemb, std::string *s)
{
    size_t newLength = size*nmemb;
    try
    {
        s->append((char*)contents, newLength);
    }
    catch(std::bad_alloc &e)
    {
        return 0;
    }
    return newLength;
}

int main() {

    int start_time = 123;
    int end_time = 231456;
    int port = 8891;
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    std::string result;
    if(curl) {
        string query;
        query = "http://127.0.0.1:"+to_string(port)+"/predict?data="+to_string(start_time)+","+to_string(end_time);
        cout<<query<<endl;
        curl_easy_setopt(curl, CURLOPT_URL, query.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlWrite_CallbackFunc_StdString);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);

        /* Perform the request, res will get the return code */ 
        res = curl_easy_perform(curl);
        /* Check for errors */ 
        if(res != CURLE_OK)
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));
        std::cout<<"Result: "<<result<<std::endl;

        curl_easy_cleanup(curl);
    }
    return 0;
}